package com.is.fixerrategateway.json.entity;

import com.is.fixerrategateway.json.model.JsonCurrencyRequest;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Entity
@Table(name = "currency_request")
@NoArgsConstructor
@Getter
@Setter
public class CurrencyRequestEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue
    private UUID id;

    @Column(name = "timestamp")
    private Long timestamp;

    @Column(name = "client")
    private int client;

    @Column(name = "currency")
    private String currency;

    public CurrencyRequestEntity(JsonCurrencyRequest currencyRequest) {
        this.id = currencyRequest.getRequestId();
        this.timestamp = currencyRequest.getTimestamp();
        this.client = currencyRequest.getClient();
        this.currency = currencyRequest.getCurrency();
    }
}
