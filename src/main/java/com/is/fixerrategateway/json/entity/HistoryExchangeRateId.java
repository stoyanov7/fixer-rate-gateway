package com.is.fixerrategateway.json.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class HistoryExchangeRateId implements Serializable {
    private String base;
    private String quote;
    private LocalDateTime timestamp;
}
