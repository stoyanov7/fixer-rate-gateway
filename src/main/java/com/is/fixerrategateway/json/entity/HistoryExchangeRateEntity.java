package com.is.fixerrategateway.json.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Table(name = "history_exchange_rate")
@IdClass(HistoryExchangeRateId.class)
@NoArgsConstructor
@Getter
@Setter
public class HistoryExchangeRateEntity {
    @Id
    @Column(name = "base")
    private String base;

    @Id
    @Column(name = "quote")
    private String quote;

    @Column(name = "rate")
    private String rate;

    @Id
    @Column(name = "timestamp")
    private LocalDateTime timestamp;

    public HistoryExchangeRateEntity(ExchangeRateEntity exRate) {
        this.base = exRate.getBase();
        this.quote = exRate.getQuote();
        this.rate = exRate.getRate();
        this.timestamp = exRate.getTimestamp();
    }
}
