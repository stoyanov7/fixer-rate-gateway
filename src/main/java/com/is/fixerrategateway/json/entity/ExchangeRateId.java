package com.is.fixerrategateway.json.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeRateId implements Serializable {
    private String base;
    private String quote;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        ExchangeRateId that = (ExchangeRateId) o;
        return base.equals(that.base) &&
                quote.equals(that.quote);
    }

    @Override
    public int hashCode() {
        return Objects.hash(base, quote);
    }
}
