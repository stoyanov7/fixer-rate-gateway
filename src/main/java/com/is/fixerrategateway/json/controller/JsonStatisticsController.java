package com.is.fixerrategateway.json.controller;

import com.is.fixerrategateway.json.model.CurrencyResponseHistory;
import com.is.fixerrategateway.json.model.CurrencyResponseCurrent;
import com.is.fixerrategateway.json.model.JsonCurrencyRequest;
import com.is.fixerrategateway.json.rabbitmq.Sender;
import com.is.fixerrategateway.json.service.ExchangeRateStatisticsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@ResponseBody
@RequestMapping("/json_api")
public class JsonStatisticsController {

    private final ExchangeRateStatisticsService exchangeRateStatisticsService;
    private final Sender sender;
    private final Logger logger = LoggerFactory.getLogger(JsonStatisticsController.class);

    @Autowired
    public JsonStatisticsController(ExchangeRateStatisticsService exchangeRateStatisticsService, Sender sender) {
        this.exchangeRateStatisticsService = exchangeRateStatisticsService;
        this.sender = sender;
    }

    @PostMapping(
        value = "/current",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CurrencyResponseCurrent> getCurrent(@RequestBody JsonCurrencyRequest currentRequest) {
        try {
            this.exchangeRateStatisticsService.validateRequestById(currentRequest.getRequestId());
            this.exchangeRateStatisticsService.createRateRequest(currentRequest);
            this.sender.sendRequestMessage(currentRequest);
            CurrencyResponseCurrent currentRates = this.exchangeRateStatisticsService.getCurrentRates(currentRequest.getCurrency());

            return ResponseEntity.ok(currentRates);
        } catch (Exception ex) {
            this.logger.error("API call failed: " + ex.getMessage());
            throw ex;
        }
    }

    @PostMapping(
        value = "/history",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CurrencyResponseHistory> getHistory(@RequestBody JsonCurrencyRequest historyRequest) {
        try {
            this.exchangeRateStatisticsService.validateRequestById(historyRequest.getRequestId());
            this.exchangeRateStatisticsService.createRateRequest(historyRequest);
            this.sender.sendRequestMessage(historyRequest);
            CurrencyResponseHistory historyRates = this.exchangeRateStatisticsService.getHistoryRates(historyRequest.getCurrency(), historyRequest.getPeriod());

            return ResponseEntity.ok(historyRates);
        } catch (Exception ex) {
            this.logger.error("API call failed: " + ex.getMessage());
            throw ex;
        }
    }
}
