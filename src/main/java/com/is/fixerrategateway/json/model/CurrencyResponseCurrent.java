package com.is.fixerrategateway.json.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class CurrencyResponseCurrent {
    private String base;
    private LocalDateTime date;
    private List<Rate> rates = new ArrayList<>();
}
