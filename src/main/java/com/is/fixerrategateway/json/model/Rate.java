package com.is.fixerrategateway.json.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@AllArgsConstructor
@Getter
@Setter
public class Rate {
    private String currency;
    private BigDecimal price;
}
