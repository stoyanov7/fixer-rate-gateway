package com.is.fixerrategateway.json.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class CurrencyResponseHistory {
    private String base;
    private List<HistoryRate> history;
    public static class HistoryRate {
        private LocalDateTime date;
        private List<Rate> rates;

        public HistoryRate(LocalDateTime date, List<Rate> rates) {
            this.date = date;
            this.rates = rates;
        }
    }
}
