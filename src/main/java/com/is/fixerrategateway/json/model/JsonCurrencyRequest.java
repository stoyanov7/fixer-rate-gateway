package com.is.fixerrategateway.json.model;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class JsonCurrencyRequest {
    private UUID requestId;
    private Long timestamp;
    private int client;
    private String currency;
    private int period;
}
