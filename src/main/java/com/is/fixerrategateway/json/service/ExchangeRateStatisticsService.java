package com.is.fixerrategateway.json.service;

import com.is.fixerrategateway.exception.RequestValidationException;
import com.is.fixerrategateway.json.entity.CurrencyRequestEntity;
import com.is.fixerrategateway.json.entity.ExchangeRateEntity;
import com.is.fixerrategateway.json.entity.HistoryExchangeRateEntity;
import com.is.fixerrategateway.json.model.CurrencyResponseCurrent;
import com.is.fixerrategateway.json.model.CurrencyResponseHistory;
import com.is.fixerrategateway.json.model.JsonCurrencyRequest;
import com.is.fixerrategateway.json.model.Rate;
import com.is.fixerrategateway.json.repository.CurrencyRequestRepository;
import com.is.fixerrategateway.json.repository.ExchangeRateRepository;
import com.is.fixerrategateway.json.repository.HistoryExchangeRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ExchangeRateStatisticsService {
    private final CurrencyRequestRepository currencyRequestRepository;
    private final ExchangeRateRepository exchangeRateRepository;
    private final HistoryExchangeRateRepository historyExchangeRateRepository;

    @Autowired
    public ExchangeRateStatisticsService(
            CurrencyRequestRepository currencyRequestRepository,
            ExchangeRateRepository exchangeRateRepository,
            HistoryExchangeRateRepository historyExchangeRateRepository) {
        this.currencyRequestRepository = currencyRequestRepository;
        this.exchangeRateRepository = exchangeRateRepository;
        this.historyExchangeRateRepository = historyExchangeRateRepository;
    }

    public void validateRequestById(UUID requestId) {
        long count = this.currencyRequestRepository.countById(requestId);

        if (count > 0) {
            throw new RequestValidationException("Request with id: " + requestId + " was already executed");
        }
    }

    public void createRateRequest(JsonCurrencyRequest exRateRequest) {
        this.currencyRequestRepository.save(new CurrencyRequestEntity(exRateRequest));
    }

    public CurrencyResponseCurrent getCurrentRates(String currency) {
        List<ExchangeRateEntity> exchangeRates = this.exchangeRateRepository.findByBase(currency);

        if (exchangeRates.isEmpty())
            return new CurrencyResponseCurrent();

        ExchangeRateEntity exchangeRate = exchangeRates.get(0);
        CurrencyResponseCurrent currencyResponseCurrent = new CurrencyResponseCurrent();
        currencyResponseCurrent.setBase(exchangeRate.getBase());
        currencyResponseCurrent.setDate(exchangeRate.getTimestamp());
        List<Rate> rates = currencyResponseCurrent.getRates();

        exchangeRates.forEach(e -> rates.add(new Rate(e.getQuote(), new BigDecimal(e.getRate()))));

        return currencyResponseCurrent;
    }

    public CurrencyResponseHistory getHistoryRates(String currency, int period) {
        LocalDateTime startingTimeStamp = LocalDateTime.now().minus(period, ChronoUnit.HOURS);
        List<HistoryExchangeRateEntity> historyExchangeRates = this.historyExchangeRateRepository.findByBaseAndInterval(currency, startingTimeStamp);

        if (historyExchangeRates.isEmpty())
            return new CurrencyResponseHistory();

        CurrencyResponseHistory historyRate = new CurrencyResponseHistory();
        HistoryExchangeRateEntity exRate = historyExchangeRates.get(0);
        historyRate.setBase(exRate.getBase());
        Map<LocalDateTime, List<Rate>> ratesByDate = this.getRatesByTimestamp(historyExchangeRates);
        List<CurrencyResponseHistory.HistoryRate> historyRates = this.getHistoryRates(ratesByDate);
        historyRate.setHistory(historyRates);

        return historyRate;
    }

    private List<CurrencyResponseHistory.HistoryRate> getHistoryRates(Map<LocalDateTime, List<Rate>> ratesByDate) {
        return ratesByDate
                .entrySet()
                .stream()
                .map(e -> new CurrencyResponseHistory.HistoryRate(e.getKey(), e.getValue()))
                .collect(Collectors.toList());
    }

    private Map<LocalDateTime, List<Rate>> getRatesByTimestamp(List<HistoryExchangeRateEntity> resultList) {
        return resultList
            .stream()
            .collect(Collectors.groupingBy(HistoryExchangeRateEntity::getTimestamp,
                Collectors.mapping(e -> new Rate(e.getQuote(), new BigDecimal(e.getRate())), Collectors.toList())
            ));
    }
}
