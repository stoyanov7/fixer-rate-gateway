package com.is.fixerrategateway.json.repository;

import com.is.fixerrategateway.json.entity.ExchangeRateEntity;
import com.is.fixerrategateway.json.entity.ExchangeRateId;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ExchangeRateRepository extends CrudRepository<ExchangeRateEntity, ExchangeRateId> {
    List<ExchangeRateEntity> findByBase(String base);
}
