package com.is.fixerrategateway.json.repository;

import com.is.fixerrategateway.json.entity.CurrencyRequestEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface CurrencyRequestRepository extends CrudRepository<CurrencyRequestEntity, UUID> {
    long countById(UUID id);
}
