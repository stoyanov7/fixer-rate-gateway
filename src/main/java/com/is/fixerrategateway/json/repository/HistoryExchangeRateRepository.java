package com.is.fixerrategateway.json.repository;

import com.is.fixerrategateway.json.entity.HistoryExchangeRateEntity;
import com.is.fixerrategateway.json.entity.HistoryExchangeRateId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface HistoryExchangeRateRepository extends CrudRepository<HistoryExchangeRateEntity, HistoryExchangeRateId> {
    @Query("FROM HistoryExchangeRateEntity h WHERE h.base=:base AND h.timestamp >=:timestamp")
    List<HistoryExchangeRateEntity> findByBaseAndInterval(@Param("base") String base, @Param("timestamp") LocalDateTime timestamp);
}
