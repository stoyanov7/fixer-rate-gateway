package com.is.fixerrategateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FixerRateGatewayApplication {
	public static void main(String[] args) {
		SpringApplication.run(FixerRateGatewayApplication.class, args);
	}
}
